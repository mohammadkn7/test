import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Form } from '../models/line/form.model';
import { Line } from '../models/line/line. model';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor() { }
    private lines = new BehaviorSubject(
        [
          new Line()
        ]
      );

  initLines() {
    return this.lines.asObservable();
  }

  changeLines(lines:any) {
    this.lines.next(lines);
   }

   saveform(form:Form) {
    let r = JSON.stringify(form);
    sessionStorage.setItem('forms', r);
   }

   getForms() {
      let Jsonform: any;
      if (sessionStorage.getItem('forms')) {
        Jsonform = sessionStorage.getItem('forms') ;
        let f= JSON.parse(Jsonform);
        return f
      }
   }


}

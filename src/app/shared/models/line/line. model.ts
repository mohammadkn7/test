import { TempOption } from "./temp-option.model";

export class Line {
  className: string ='col-xs-12';
  name: string = '';
  type: string = 'blank';
  option: TempOption = new TempOption();
}

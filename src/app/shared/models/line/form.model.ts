import { Line } from "./line. model";

export class Form {
  name: string ='';
  lines: Line[] = [];
}

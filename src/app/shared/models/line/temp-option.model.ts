export class TempOption {
    type: string = '';
    typeInput: string = '';
    required: boolean = false;
    label: string = '';
    description: string = '';
    validator: string = '';
    showFormat: string = '';
    role: string[] = [];
    option:Option[] = [];
}

class Option {
  value?:string ;
  label?:string ;
}

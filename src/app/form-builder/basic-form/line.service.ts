import { Injectable } from '@angular/core';
import { Line } from 'src/app/shared/models/line/line. model';

@Injectable({
  providedIn: 'root'
})
export class LineService {

  constructor() { }
   i =10;
  add(lines:any){
    let line:Line = new Line();
    lines.push (
      line
    )
  }

  move(fromIndex: number, toIndex: number, lines:any) {
    let arr = lines;
    let element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
  }

  delete(i:number, lines:any){
    let arr = lines;
    arr.splice(i, 1);
  }
}

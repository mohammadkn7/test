import { Component, OnInit } from '@angular/core';
import { Line } from 'src/app/shared/models/line/line. model';
import { FormService } from 'src/app/shared/services/form.service';
import { LineService } from './line.service';

@Component({
  selector: 'app-basic-form',
  templateUrl: './basic-form.component.html',
  styleUrls: ['./basic-form.component.scss']
})
export class BasicFormComponent implements OnInit {

  constructor(private $lineService: LineService,
              private $formService: FormService) {
                this.$formService.initLines().subscribe(
                  data => {
                    this.lines = [...data];
                   }
                )
              }


  lines : Line[] = [new Line()];

  ngOnInit(): void {
  }

  addLines() {
    this.$lineService.add(this.lines);
    this.$formService.changeLines(this.lines);
  }

  moveLine(fromIndex: number, toIndex: number) {
    this.$lineService.move(fromIndex,toIndex, this.lines);
    this.$formService.changeLines(this.lines);
  }

  deleteLine(i:number){
    this.$lineService.delete(i, this.lines);
    this.$formService.changeLines(this.lines);
  }

}

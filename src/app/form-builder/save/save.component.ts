import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Form } from 'src/app/shared/models/line/form.model';
import { Line } from 'src/app/shared/models/line/line. model';
import { FormService } from 'src/app/shared/services/form.service';

@Component({
  selector: 'app-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.scss']
})
export class SaveComponent implements OnInit, OnDestroy {

  constructor(private $formService: FormService,
              private _snackBar: MatSnackBar) { }

  formsName = '';
  lines : Line[] = [new Line()];
  subscription!: Subscription


  ngOnInit(): void {
    this.subscription = this.$formService.initLines().subscribe(
      data => {
        this.lines = data;
      }
    )
  }

  save() {
    let form: Form = new Form();
    form.name = this.formsName;
    form.lines = this.lines;
    this.$formService.saveform(form);
    this.openSnackBar('form saved successfully','ok');
  }

  openSnackBar(message: string, action?: string) {
    this._snackBar.open(message, action,
       {
        horizontalPosition: 'start',
        verticalPosition : 'bottom',
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}

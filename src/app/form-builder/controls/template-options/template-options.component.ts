import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
 import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Line } from 'src/app/shared/models/line/line. model';
import { FormService } from 'src/app/shared/services/form.service';
import { ValidationService } from 'src/app/shared/services/validation.service';

@Component({
  selector: 'app-template-options',
  templateUrl: './template-options.component.html',
  styleUrls: ['./template-options.component.scss']
})
export class TemplateOptionsComponent implements OnInit, OnDestroy {

  constructor(@Inject(MAT_DIALOG_DATA) public data:any,
              private $formService: FormService,
              private $validationService: ValidationService ) {

    this.index = data.index;
    console.log(this.index);
  }

  lines : Line[] = [new Line()];
  index: number = 0;
  typeSelected = 'None';
  validators:any;
  roleList: string[] = ['Admin', 'Guest', 'customer'];
  roles:any;
  radiolabel = ''; radioValue = '';
  optionlabel = ''; optionValue = '';
  subscription!: Subscription

  ngOnInit(): void {
    this.subscription=this.$formService.initLines().subscribe(
      data => {
        console.log(data,this.index);
        this.lines = [...data]
      }
    );

    this.getValidations();
  }

  getValidations() {
    this.validators = this.$validationService.getValidators();
  }

  addNewOption() {
    this.lines[this.index].option.option.push({
      value: this.optionValue,
      label: this.optionlabel
    })
    this.optionValue = '';
    this.optionlabel = '';
  }
  addNewRadio() {
    this.lines[this.index].option.option.push({
      value: this.radioValue,
      label: this.radiolabel
    })
    this.radioValue = '';
    this.radiolabel = '';
  }
  // register line in form
  register() {
    console.log(this.roles);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}

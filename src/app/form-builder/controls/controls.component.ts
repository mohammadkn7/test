import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Line } from 'src/app/shared/models/line/line. model';
import { FormService } from 'src/app/shared/services/form.service';
import { TemplateOptionsComponent } from './template-options/template-options.component';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss']
})
export class ControlsComponent implements OnInit {

  constructor(private $formService: FormService,
              public dialog: MatDialog) {


   }

  ngOnInit(): void {
    this.subscription = this.$formService.initLines().subscribe(
      data => {

        this.lines = [...data];
      }
    )
  }

  formsName= 'form';
  selectedIndex = 0;
  lines : Line[] = [new Line()];
  subscription!: Subscription


  openDialog(i:number) {
    this.selectedIndex = i;
    this.dialog.open(TemplateOptionsComponent, {
      data: {
        index:i
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}

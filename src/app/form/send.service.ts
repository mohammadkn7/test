import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SendService {

  constructor() { }

  firstFn() {
    let o = new Observable((o) => {
      setTimeout(() => {
        let m: string = 'first function done'
        o.next(m)
      }, 1000);
    })
    return o;
  }

  save(form:any) {
    // send to server by a post method
    console.log(form);
    let o = new Observable((o) => {
      setTimeout(() => {
        o.next('send data to server done')
      }, 3000)
    })
    return o;
  }

}

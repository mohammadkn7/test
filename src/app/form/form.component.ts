import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Form } from '../shared/models/line/form.model';
import { FormService } from '../shared/services/form.service';
import { ValidationService } from '../shared/services/validation.service';
import { SendService } from './send.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent  {

  constructor(private $formService: FormService,
              private formBuilder: FormBuilder,
              private _snackBar: MatSnackBar,
              private $validatonService: ValidationService,
              private $sendService: SendService) {

    this.form = this.getForm();
    this.getFormGp();
   }

   message1: any;message2: any; message3: any;
   subscription1!: Subscription; subscription2!: Subscription;
   form:Form;
   formGroup:any;
   res: any; keys: any;
   role :any = 'Admin';
   roleList: any[] = ['Admin', 'Guest', 'customer'];

  getForm() {
    let r =  this.$formService.getForms();
    return r;
  }

  register() {
    if(this.formGroup.status == 'VALID') {
        this.firstFn();
      }
  }

  send() {
    this.subscription2 = this.$sendService.save(this.formGroup.value)
      .subscribe(
        d => {
          this.message2 = d;
          this.openSnackBar(d, 'got it');
          console.log("2 done");
          this.lastFn();
          this.res = Object.values(this.formGroup.value);
          this.keys = Object.keys(this.formGroup.value);
        },
        e => {console;e.log(e)},
        () => {
        }
      )

  }

  firstFn() {
    this.subscription1 = this.$sendService.firstFn()
    .subscribe(
      d => {
        this.message1 = d;
        console.log("1 done");
        this.send();},
      e => {console.log(e);},
      () => {
      }
    )
  }

  lastFn() {
    setTimeout(() => {
      this.message3 = ' last function done';
    }, 500);
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  getFormGp() {

    let filed = this.form.lines;
    let required !:Validators;
    let validator !:any;
    console.log('lines=', filed);
    let group:any={};

    filed.forEach((line) => {
      if (line.option.required) {
        required = Validators.required;
      } else {
        required = () => true;
      }

      switch (line.option.validator) {
        case 'email':
          validator = this.$validatonService.emailValidator;
          break;
        case 'password':
          validator = this.$validatonService.passwordValidator;
          break;

        default:
          validator = () => true;
          break;
      }
      group[line.name] = ['', [required, validator]];
    });
    this.formGroup = this.formBuilder.group(group);
  }

  roleHandle(filedRole:any)  {
    if(this.role == 'Admin' || this.role == 'customer') {
      return true
    } else if (filedRole[0] == 'Guest') {
      return true
    }
    else {
    return false;
    }
  }

  disableHandle(filedRole:any) {
    if(this.role == "Admin") return false;
    if(this.role == 'customer' && filedRole[0] == 'Admin' ) {
      return true;
    }
    return false
  }

  openSnackBar(message: any, action?: string) {
    this._snackBar.open(message, action,
       {
        horizontalPosition: 'start',
        verticalPosition : 'bottom',
      });
  }

}

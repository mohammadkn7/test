import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilderComponent } from './form-builder/form-builder.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { BasicFormComponent } from './form-builder/basic-form/basic-form.component';
import { ControlsComponent } from './form-builder/controls/controls.component';
import { TemplateOptionsComponent } from './form-builder/controls/template-options/template-options.component';
import { SaveComponent } from './form-builder/save/save.component';
import { FormComponent } from './form/form.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HelpComponent } from './help/help.component';


@NgModule({
  declarations: [
    AppComponent,
    FormBuilderComponent,
    BasicFormComponent,
    ControlsComponent,
    TemplateOptionsComponent,
    SaveComponent,
    FormComponent,
    NavbarComponent,
    HelpComponent,

  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    NoopAnimationsModule,
    AngularMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

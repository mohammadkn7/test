import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormBuilderComponent } from './form-builder/form-builder.component';
import { FormComponent } from './form/form.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
  {path:'',redirectTo: 'Form-Generator', pathMatch: 'full'},
  {path:'Form-Generator',component:FormBuilderComponent},
  {path:'Form',component:FormComponent},
  {path:'Help',component:HelpComponent},


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
